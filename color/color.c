#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curses.h>

#include <miniat/miniat.h>
#include "store_text.h"
#include "text_length.h"
#include "window_x.h"
#include "color.h"

struct color {
	int connected;
	m_uword address;
	m_bus *bus;
	
};
int s_color;
int n = 1;

color *color_new(m_uword address) {
		
		
	

	color *t = malloc(sizeof(color));
	if(t) {		
		t->bus = (m_bus *)malloc(sizeof(m_bus));
		if(!t->bus) {
			free(t);
			t = NULL;
		}
		else {
			t->connected = 0;
			t->address = address;
		
		}
	}
	return t;
}

void color_free(color *t) {

	if(t) {
		if(!t->connected) {
			free(t->bus);
		}
		free(t);
	}
	return;
}

void color_clock(color *t) {
	start_color();//begins colour selection

	if ( has_colors() && COLOR_PAIRS >= 2 ) {//if there are multiple colour pairs

	init_pair(1,  COLOR_RED,  COLOR_BLACK);//sets first colour pair
	init_pair(2,  COLOR_GREEN,  COLOR_BLACK);//sets second colour pair
    init_pair(3,  COLOR_YELLOW,  COLOR_BLACK);
    init_pair(4,  COLOR_BLUE,  COLOR_BLACK);
    init_pair(5,  COLOR_MAGENTA,  COLOR_BLACK);
    init_pair(6,  COLOR_CYAN,  COLOR_BLACK);
    init_pair(7,  COLOR_WHITE,  COLOR_BLACK);
	}
	if(s_color<8)
	{
		color_set(s_color, NULL);//sets up colour set with value of s_color and NULL
	}
	else
	{
		color_set(n, NULL);
		n++;
		if(n==8)
		{
			n=1;
		}
	}
	if(!t) {
		return;
			}
	
	/*
	 * If the miniat is making a request, has the address set and is writing
	 * we ack that we have received the request and take the data off the bus
	 */
	if(t->bus->req && (t->bus->address == t->address) && !t->bus->ack) {
		t->bus->ack = M_HIGH;
		if(t->bus->rW){	 //if rW flag is set in bus
			if((s_color >= 0) && (s_color < 16)){ //if colour variable is inside of range of the colour variables
				s_color = t->bus->data; //gets the variable from the bus
			}
			else{
				printw("Number out of range\n");
			}
        }
        else{      
        			
        		t->bus->data=(char)s_color;       //sends a character of the set colour through the bus 	
       	 	}
       	 	
        }
	
	
	/* If we ack'd bring it back low */
	else if((t->bus->address == t->address) && t->bus->ack) {
		t->bus->ack = M_LOW;
	}
	return;
}

m_bus color_get_bus(color *t) {
	m_bus b = { 0 };
	if(!t) {
		return b;
	}
	return *(t->bus);
}

void color_set_bus(color * t, m_bus bus) {

	if(t) {
		memcpy(t->bus, &bus, sizeof(bus));
	}
}
