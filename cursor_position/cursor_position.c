#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curses.h>

#include <miniat/miniat.h>
#include "cursor_position.h"

struct p_cursor_pos {
	int connected;
	m_uword address;
	m_bus *bus;	
};

int position=0;

p_cursor_pos *p_cursor_pos_new(m_uword address) {
		
		
	

	p_cursor_pos  *t = malloc(sizeof(p_cursor_pos ));
	if(t) {

		

		t->bus = (m_bus *)malloc(sizeof(m_bus));
		if(!t->bus) {
			free(t);
			t = NULL;
		}
		else {
			t->connected = 0;
			t->address = address;		
			
		}
	}
	return t;
}

void p_cursor_pos_bus_connector_set(p_cursor_pos  *t, m_bus *bus) {

	if(t && bus) {
		if(!t->connected) {
			free(t->bus);
		}
		t->bus = bus;
		t->connected = 1;
	}
	return;
}

void p_cursor_pos_free(p_cursor_pos *t) {

	if(t) {
		if(!t->connected) {
			free(t->bus);
		}
		free(t);
	}
	return;
}

void p_cursor_pos_clock(p_cursor_pos *t) {
	
	if(!t) {
		return;
			}
	
	/*
	 * If the miniat is making a request, has the address set and is writing
	 * we ack that we have received the request and take the data off the bus
	 */
	if(t->bus->req && (t->bus->address == t->address) && !t->bus->ack) {
		t->bus->ack = M_HIGH;
		if(t->bus->rW){	//if rW flag is set in bus
			if((position >= 0) && (position < 16)){ //if position variable is within range
				position = t->bus->data; //gets position variable from bus
			}
			else{
				printw("Number out of range\n");
			}
        }
        else{        	
        		t->bus->data=position;       // sends position variable through bus
       	 	}
       	 	
        }
       
	
	
	
	/* If we ack'd bring it back low */
	else if((t->bus->address == t->address) && t->bus->ack) {
		t->bus->ack = M_LOW;
	}
	return;
}

m_bus p_cursor_pos_get_bus(p_cursor_pos *t) {
	m_bus b = { 0 };
	if(!t) {
		return b;
	}
	return *(t->bus);
}

void p_cursor_pos_set_bus(p_cursor_pos * t, m_bus bus) {

	if(t) {
		memcpy(t->bus, &bus, sizeof(bus));
	}
}
