# MiniAT Signboard

## Elements

### Window area
- 100 x 30 pixel screen 
- 0,0 is at the top-left corner
### Text Font
- 8x7 pixels, custom font for standard A-Z, a-z, 0-9 and ASCII characters.
- Can choose which ASCII character is used to print the letters and symbols
### Text Color
- White
- Red
- Green
- Yellow
- Blue
- Magenta
- Cyan 
### Directional buttons
- 4 directional buttons, up, down, right, and left
- These are mapped to the arrow keys on the keyboard.
### Power Toggle
- MiniAT Signboard is set to start by default; the following may be used to toggle the power:
     - "o" will turn stop the signboard, while leaving the program running
     - "p" will turn the signboard on if it is in the off state

## Memory Map
### Directional Buttons
- address 0x4000
- LOAD - Loads number associated with which button was pressed:
    - 0 for no button
    - 1 for up button
    - 2 for down button
    - 3 for left button
    - 4 for right button

### Cursor Print 
- adress   0x4001
- STOR – Implements letter recognition function and prints out character on the screen of window_x position.

### Store Text buffer (array [15])
- address  0x4002
- STOR – Stores character into an array, with index position Cursor_Position.
- LOAD – Loads character from an array with index position of Cursor_Position.

### Cursor Position 
- address 0x4003
- STOR – Sets Cursor Position.
- LOAD - Loads Cursor Position

### Text Length
- address 0x4004
- STOR – Sets text length variable.
- LOAD – Loads text variable

### Window X coordinate
- address 0x4005
- STOR - Sets y cursor position to a specific point on the screen
- LOAD - Loads window_x variable.

### Clear Screen
- address 0x4010
- STOR – Clears an entire screen

### X_Char –  for user convenient,  x_char sets window x=value*6.  
- address 0x4011
- STOR – Sets x_char to the number specified and sets window_x coordinate to that number multiplied by 6.
- LOAD – Returns current x_char position.

### Buffer_print 
- address 0x4012
- STOR – Prints out what Store_Text Buffer contains. On 0 – prints everything, otherwise print amount of character specified.

### Column
- address 0x4013
- STOR - Sets X coordinate of column for use in printing a single column.

### Row
- address 0x4014
- STOR - Sets Y Coordinate of row for use in printing a single row.

### Window Y coordinate
- address 0x4015
- STOR - Sets y cursor position to a specific point on the screen
- LOAD - Loads currently set y cursor position
 
### Color
- address 0x4016
- STOR - Sets current color to use when printing text, using the scheme defined above.

### Font Character
- address 0x4017
- STOR - Sets ASCII character to use when printing out font characters

### Print Single Character
- address 0x4018
- STOR - Outputs character being stored at the current positions of Window X and Window Y.

### Buffer_Curve
- address 0x4019  
- STOR - Sets value for characters variable.
