#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <curses.h>
#include "font.h"
#include "fontCharacter.h"

#define BIT_OFF ' '
//#define BIT_ON  '@'


font *load_font(char *filename) {

	font *result = NULL;
	FILE *file;
	size_t count;
	char *line;
	int w;
	int h;
	bool is_bad;
	int letter;
	int row;
	int col;

	if((file = fopen(filename, "r")) == NULL) {
        	printf("Error loading font.  This may be because the SIGNBOARDPATH environment variable is defined incorrectly, or because the custom font you defined is invalid.  See README.md in the Bitbucket Repository for more info.\n");
		exit(1);
	}

	is_bad = false;
	do {
		int letter_row;
		int letter_col;
        int i;

		/* the first line should be the magic string "P1" */
        count = 0;
        if(getline(&line, &count, file) < 0) { is_bad = true; free(line); break; }
        if(strcmp(line, "P1\n") != 0)          { is_bad = true; free(line); break; }
        free(line); // must free after getline
        line = NULL;

		/* 2nd line is a # comment */
        count = 0;
        if(getline(&line, &count, file) < 0) { is_bad = true; free(line); break; }
		if(!line || line[0] != '#')          { is_bad = true; free(line); break; }
        free(line); // must free after getline
        line = NULL;

		/* 3rd line is width and height */
        count = 0;
        if(getline(&line, &count, file) < 0)   { is_bad = true; free(line); break; }
		if(sscanf(line, "%d %d", &w, &h) != 2) { is_bad = true; free(line); break; }
		free(line); // must free after getline
        line = NULL;

		result = (font *)malloc(sizeof(font));
		if(!result) { is_bad = true; break; }

        if((w % PBM_DIMENSION_SIZE != 0) || (h % PBM_DIMENSION_SIZE != 0)) { is_bad = true; break; }
        result->width = w / PBM_DIMENSION_SIZE;
        result->height = h / PBM_DIMENSION_SIZE;

		/*
         * Allocate space for all the pointers to letter rows
		 */
        result->letters[0] = malloc(sizeof(char **) * (PBM_CHAR_CNT * result->height));
        if(!result->letters) { is_bad = true; break; }

        for(letter = 0; letter < PBM_CHAR_CNT; letter++) {
            result->letters[letter] = result->letters[0] + (letter * result->height);
        }

        /*
         * Allocate space for all the character data in the letters
         */
        result->letters[0][0] = calloc(sizeof(char), PBM_CHAR_CNT * result->height * (result->width + 1));
        if(!result->letters[0][0]) { is_bad = true; break; }

        for(i = 0; i < PBM_CHAR_CNT * result->height; i++) {
            result->letters[0][i] = result->letters[0][0] + (i * (result->width + 1));
        }
        //char BIT_ON=useCharacter;
		/* 
		 * So far so good. There should now be a stream of (w * h) 0s and 1s
		 * with arbitrary whitespace. After this could be anything, so we'll
		 * only concern ourselves with finding (w * h) 0s and 1s next.
		 */
        for(letter_row = 0; letter_row < PBM_DIMENSION_SIZE; letter_row++) {
            int poo = 0;
            for(row = 0; row < result->height; row++) {
                for(letter_col = 0; letter_col < PBM_DIMENSION_SIZE; letter_col++) {
                    for(col = 0; col < result->width; col++) {
                        int c;

                        do {
                            c = fgetc(file);
                            switch(c) {
                            case (int)'0':
                            case (int)'1':
                            case (int)' ':
                            case (int)'\n':
                            case (int)'\r':
                            case (int)'\f':
                            case (int)'\v':
                            case (int)'\t':
                            case (int)'\b':
                                break;
                            default:
                                is_bad = true;
                                break;
                            }
                        } while(c != '0' && c != '1');

                        letter = (letter_row * PBM_DIMENSION_SIZE) + letter_col;
                        poo++;
//                        printf("%d\n", poo);
                        fflush(NULL);
                        result->letters[letter][row][col] = (c == '0' ? BIT_OFF : useCharacter);
					}
				}
			}
		}

	} while(false);


	/*
	 * If anything went bad, attempt to free everything and return NULL.
	 */
	if(is_bad) {
		fprintf(stderr, "%s doesn't appear to be a valid PBM font.\n", filename);
		fprintf(stderr, "Fonts should be in ASCII-PBM with 16 x 16 monospaced letters.\n");
		fprintf(stderr, "Currently, only the Gimp PBM export filter is read.\n");
		fprintf(stderr, "\n... something else might have gone wrong, though :-P\n");

		free_font(result);
		result = NULL;
	}

	fclose(file);
	return result;
}

void free_font(font *f) {
    if(f) {
        if(f->letters[0]) {
            if(f->letters[0][0]) {
                free(f->letters[0][0]);
            }
            free(f->letters[0]);
        }
        free(f);
    }
}

void print_font_string(font *f, char *str, int x, int y) {
	char *s;
	int row;

	if(!f || !str) { return; }

	for(row = 0; row < f->height; row++) {
		s = str;

		while(*s != '\0') {

			mvprintw(y, x, "%s",f->letters[(int)*s][row]);
			s++;

		}
        y++;
		
	}
	fflush(NULL);
}
void print_font_char(font *f, char *ch, int x, int y) {
    char *s;
    int row;

    if(!f || !ch) { return; }

    for(row = 0; row < f->height; row++) {
        s = ch;
        mvprintw(y, x, "%s",f->letters[(int)*s][row]);
        y++;
        
    }
    fflush(NULL);
}

void print_font_sheet(font *f) {
	int letter;
	int letter_row;
	int letter_col;
	int row;

    for(letter_row = 0; letter_row < PBM_DIMENSION_SIZE; letter_row++) {
		for(row = 0; row < f->height; row++) {
            for(letter_col = 0; letter_col < PBM_DIMENSION_SIZE; letter_col++) {
                letter = (letter_row * PBM_DIMENSION_SIZE) + letter_col;
				printf("%s", f->letters[letter][row]);
			}
			printf("\n");
            fflush(NULL);
            {
                int i = 60000000;
                while(i > 0) i--;
            }
		}
	}
}
