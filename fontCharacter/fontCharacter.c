#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curses.h>

#include <miniat/miniat.h>
#include "fontCharacter.h"
#include "font.h"

struct fontChar {
	int connected;
	m_uword address;
	m_bus *bus;
	
};
char useCharacter='a';
font *ourfont;
char *pPath;

fontChar *fontCharacter_new(m_uword address) {
		
		
	

	fontChar *t = malloc(sizeof(fontChar));
	if(t) {	


		t->bus = (m_bus *)malloc(sizeof(m_bus));
		if(!t->bus) {
			free(t);
			t = NULL;
		}
		else {
			t->connected = 0;
			t->address = address;
		
		}
	}
	return t;
}

void fontCharacter_free(fontChar *t) {

	if(t) {
		if(!t->connected) {
			free(t->bus);
			free_font(ourfont);
		}
		free(t);
	}
	return;
}

void fontCharacter_clock(fontChar *t) {
	if(!t) {
		return;
			}
	
	/*
	 * If the miniat is making a request, has the address set and is writing
	 * we ack that we have received the request and take the data off the bus
	 */
	if(t->bus->req && (t->bus->address == t->address) && !t->bus->ack) {
		t->bus->ack = M_HIGH;
		if(t->bus->rW){	
			if(t->bus->data){
				
				useCharacter = t->bus->data;
				pPath = getenv("SIGNBOARDPATH");
				ourfont = load_font(pPath);
				
			}
        }
        else{      
        			
        		t->bus->data=(char)useCharacter;        	
       	 	}
       	 	
        }
	
	
	/* If we ack'd bring it back low */
	else if((t->bus->address == t->address) && t->bus->ack) {
		t->bus->ack = M_LOW;
	}
	return;
}

m_bus fontCharacter_get_bus(fontChar *t) {
	m_bus b = { 0 };
	if(!t) {
		return b;
	}
	return *(t->bus);
}

void fontCharacter_set_bus(fontChar * t, m_bus bus) {

	if(t) {
		memcpy(t->bus, &bus, sizeof(bus));
	}
}
