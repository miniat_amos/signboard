#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>                  /*  for sleep()  */
#include <curses.h>
#include "lettersPrint.h"


void print_letters(int CX, int CY, char CAlpha)
{
	
	
	char X = 'o';
	//Fix lowercase A and lowercase S
	//Working:
	//A-Z,a-z,0-9
	if(CAlpha == 'A')
	{
		//Row 1
		mvaddch(CY,CX+2,X);
		//Row 2
		mvaddch(CY+1,CX+1,X);
		mvaddch(CY+1,CX+3,X);
		//Row 3
		mvaddch(CY+2,CX,X);
		mvaddch(CY+2,CX+4,X);
		//Row 4
		mvaddch(CY+3,CX,X);
		mvaddch(CY+3,CX+1,X);
		mvaddch(CY+3,CX+2,X);
		mvaddch(CY+3,CX+3,X);
		mvaddch(CY+3,CX+4,X);         
		//Row 5
		mvaddch(CY+4,CX+0,X);
		mvaddch(CY+4,CX+4,X);
	}
	else if(CAlpha == 'a')
	{
		//Row 1
		mvaddch(CY,CX+1,X);
		mvaddch(CY,CX+2,X);
		mvaddch(CY,CX+4,X);
		//Row 2
		mvaddch(CY+1,CX,X);
		mvaddch(CY+1,CX+3,X);
		mvaddch(CY+1,CX+4,X);
		//Row 3
		mvaddch(CY+2,CX,X); 
		mvaddch(CY+2,CX+4,X);
		//Row 4
		mvaddch(CY+3,CX,X);
		mvaddch(CY+3,CX+3,X); 
		mvaddch(CY+3,CX+4,X);         
		//Row 5
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
		mvaddch(CY+4,CX+4,X);
	}
	else if(CAlpha == 'B')
	{
		// B CODE
		mvaddch(CY+0,CX+0,X);
		mvaddch(CY+0,CX+1,X);
		mvaddch(CY+0,CX+2,X);
		//Row 2
		mvaddch(CY+1,CX+0,X);
		mvaddch(CY+1,CX+3,X);

		//Row 3
		mvaddch(CY+2,CX+0,X);
		mvaddch(CY+2,CX+1,X);
		mvaddch(CY+2,CX+2,X);

		//Row 4
		mvaddch(CY+3,CX+0,X);
		mvaddch(CY+3,CX+3,X);

		//Row 5
		mvaddch(CY+4,CX+0,X);
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
	}
	else if(CAlpha == 'b')
	{
		// b CODE
		mvaddch(CY+0,CX+0,X);
		//Row 2
		mvaddch(CY+1,CX+0,X);

		//Row 3
		mvaddch(CY+2,CX+0,X);
		mvaddch(CY+2,CX+1,X);
		mvaddch(CY+2,CX+2,X);

		//Row 4
		mvaddch(CY+3,CX+0,X);
		mvaddch(CY+3,CX+3,X);

		//Row 5
		mvaddch(CY+4,CX+0,X);
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
	}
	else if(CAlpha == 'c' || CAlpha == 'C')
	{
		// C CODE
		// Row 1
		// Row 2
		mvaddch(CY+1,CX+1,X);
		mvaddch(CY+1,CX+2,X);
		// Row 3
		mvaddch(CY+2,CX,X);
		// Row 4
		mvaddch(CY+3,CX,X);
		// Row 5
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
	}
	else if(CAlpha == 'D')
	{
		// D CODE
		//Row 1
		mvaddch(CY+0,CX+0,X);
		mvaddch(CY+0,CX+1,X);
		mvaddch(CY+0,CX+2,X);
		mvaddch(CY+0,CX+3,X);

		//Row 2
		mvaddch(CY+1,CX+0,X);
		mvaddch(CY+1,CX+4,X);

		//Row 3
		mvaddch(CY+2,CX+0,X);
		mvaddch(CY+2,CX+4,X);

		//Row 4
		mvaddch(CY+3,CX+0,X);
		mvaddch(CY+3,CX+4,X);

		//Row 5
		mvaddch(CY+4,CX+0,X);
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
		mvaddch(CY+4,CX+3,X);

	}
	else if(CAlpha == 'd')
	{
		// d CODE
			//Row 1
		mvaddch(CY+0,CX+3,X);

		//Row 2
		mvaddch(CY+1,CX+3,X);

		//Row 3
		mvaddch(CY+2,CX+1,X);
		mvaddch(CY+2,CX+2,X);
		mvaddch(CY+2,CX+3,X);

		//Row 4
		mvaddch(CY+3,CX+0,X);
		mvaddch(CY+3,CX+3,X);

		//Row 5
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
		mvaddch(CY+4,CX+3,X);

	}
	else if(CAlpha == 'E')
	{
		// E CODE
		// Row 1
		mvaddch(CY,CX,X);
		mvaddch(CY,CX+1,X);
		mvaddch(CY,CX+2,X);
		mvaddch(CY,CX+3,X);
		// Row 2
		mvaddch(CY+1,CX,X);
		// Row 3
		mvaddch(CY+2,CX,X);
		mvaddch(CY+2,CX+1,X);
		mvaddch(CY+2,CX+2,X);
		// Row 4
		mvaddch(CY+3,CX,X);
		// Row 5
		mvaddch(CY+4,CX,X);
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
		mvaddch(CY+4,CX+3,X);
	}
	else if(CAlpha == 'e')
	{
		// E CODE
		// Row 1
		mvaddch(CY,CX+1,X);
		mvaddch(CY,CX+2,X);
		mvaddch(CY,CX+3,X);
		// Row 2
		mvaddch(CY+1,CX,X);
		mvaddch(CY+1,CX+4,X);
		// Row 3
		mvaddch(CY+2,CX,X);
		mvaddch(CY+2,CX+1,X);
		mvaddch(CY+2,CX+2,X);
		mvaddch(CY+2,CX+3,X);
		mvaddch(CY+2,CX+4,X);
		// Row 4
		mvaddch(CY+3,CX,X);
		// Row 5
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
		mvaddch(CY+4,CX+3,X);
		mvaddch(CY+4,CX+4,X);
	}
	else if(CAlpha == 'F')
	{
		// F CODE
		// Row 1
		mvaddch(CY,CX,X);
		mvaddch(CY,CX+1,X);
		mvaddch(CY,CX+2,X);
		// Row 2
		mvaddch(CY+1,CX,X);
		// Row 3
		mvaddch(CY+2,CX,X);
		mvaddch(CY+2,CX+1,X);
		mvaddch(CY+2,CX+2,X);
		// Row 4
		mvaddch(CY+3,CX,X);
		// Row 5
		mvaddch(CY+4,CX,X);
	}
	else if(CAlpha == 'f')
	{
		// f CODE
		// Row 1
		mvaddch(CY,CX+2,X);
		mvaddch(CY,CX+3,X);
		// Row 2
		mvaddch(CY+1,CX,X);
		// Row 3
		mvaddch(CY+2,CX,X);
		mvaddch(CY+2,CX+1,X);
		mvaddch(CY+2,CX+2,X);
		// Row 4
		mvaddch(CY+3,CX+1,X);
		// Row 5
		mvaddch(CY+4,CX+1,X);
	}
	else if(CAlpha == 'G')
	{
		//G CODE
		// Row 1
		mvaddch(CY,CX+1,X);
		mvaddch(CY,CX+2,X);
		// Row 2
		mvaddch(CY+1,CX,X);
		// Row 3
		mvaddch(CY+2,CX,X);
		mvaddch(CY+2,CX+2,X);
		mvaddch(CY+2,CX+3,X);
		mvaddch(CY+2,CX+4,X);
		// Row 4
		mvaddch(CY+3,CX,X);
		mvaddch(CY+3,CX+3,X);
		// Row 5
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
	}
	else if(CAlpha == 'g')
	{
		//G CODE
		// Row 1
		mvaddch(CY,CX+1,X);
		mvaddch(CY,CX+2,X);
		mvaddch(CY,CX+3,X);
		// Row 2
		mvaddch(CY+1,CX,X);
		mvaddch(CY+1,CX+3,X);
		// Row 3
		mvaddch(CY+2,CX+1,X);
		mvaddch(CY+2,CX+2,X);
		mvaddch(CY+2,CX+3,X);
		// Row 4
		mvaddch(CY+3,CX+3,X);
		// Row 5
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
		mvaddch(CY+4,CX+3,X);
	}
	else if(CAlpha == 'H')
	{
		// H CODE
		// Row 1
		mvaddch(CY,CX, X);
		mvaddch(CY,CX+4, X);
		// Row 2
		mvaddch(CY+1,CX, X);
		mvaddch(CY+1,CX+4, X);
		// Row 3
		mvaddch(CY+2,CX,X);
		mvaddch(CY+2,CX+1,X);
		mvaddch(CY+2,CX+2,X);
		mvaddch(CY+2,CX+3,X);
		mvaddch(CY+2,CX+4,X);
		// Row 4
		mvaddch(CY+3,CX,X);
		mvaddch(CY+3,CX+4,X);
		// Row 5
		mvaddch(CY+4,CX,X);
		mvaddch(CY+4,CX+4,X);
	}
	else if(CAlpha == 'h')
	{
		// h CODE
		// Row 1
		mvaddch(CY,CX, X);
		// Row 2
		mvaddch(CY+1,CX, X);
		// Row 3
		mvaddch(CY+2,CX,X);
		mvaddch(CY+2,CX+1,X);
		mvaddch(CY+2,CX+2,X);
		mvaddch(CY+2,CX+3,X);
		// Row 4
		mvaddch(CY+3,CX,X);
		mvaddch(CY+3,CX+4,X);
		// Row 5
		mvaddch(CY+4,CX,X);
		mvaddch(CY+4,CX+4,X);
	}
	else if(CAlpha == 'I')
	{
		// I CODE	
		// Row 1
		mvaddch(CY,CX,X);
		mvaddch(CY,CX+1,X);
		mvaddch(CY,CX+2,X);
		mvaddch(CY,CX+3,X);
		mvaddch(CY,CX+4,X);
		// Row 2
		mvaddch(CY+1,CX+2,X);
		// Row 3
		mvaddch(CY+2,CX+2,X);
		// Row 4
		mvaddch(CY+3,CX+2,X);
		// Row 5
	 	mvaddch(CY+4,CX,X);
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
		mvaddch(CY+4,CX+3,X);
		mvaddch(CY+4,CX+4,X);
	}
	else if(CAlpha == 'i')
	{
		// I CODE	
		// Row 1
		mvaddch(CY,CX+2,X);
		// Row 2
		// Row 3
		mvaddch(CY+2,CX+2,X);
		// Row 4
		mvaddch(CY+3,CX+2,X);
		// Row 5
		mvaddch(CY+4,CX+2,X);
	}
	else if(CAlpha == 'J')
	{
		// J CODE
		// Row 1
		mvaddch(CY,CX,X);
		mvaddch(CY,CX+1,X);
		mvaddch(CY,CX+2,X);
		// Row 2
		mvaddch(CY+1,CX+1,X);
		// Row 3
		mvaddch(CY+2,CX+1,X);
		// Row 4
		mvaddch(CY+3,CX+1,X);
		mvaddch(CY+3,CX+3,X);
		// Row 5
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
	}
	else if(CAlpha == 'j')
	{
		// j CODE
		// Row 1
		mvaddch(CY,CX+3,X);
		// Row 2
		mvaddch(CY+1,CX+3,X);
		// Row 3
		mvaddch(CY+2,CX+3,X);
		// Row 4
		mvaddch(CY+3,CX+0,X);
		mvaddch(CY+3,CX+3,X);
		// Row 5
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
	}
	else if(CAlpha == 'K')
	{
		//Letter K
		//Row 1
		mvaddch(CY+0,CX+1,X);
		mvaddch(CY+0,CX+4,X);
		//Row 2
		mvaddch(CY+1,CX+1,X);
		mvaddch(CY+1,CX+3,X);
		//Row 3
		mvaddch(CY+2,CX+1,X);
		mvaddch(CY+2,CX+2,X);
		//Row 4
		mvaddch(CY+3,CX+1,X);
		mvaddch(CY+3,CX+3,X);
		//Row 5
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+4,X);
	}
	else if(CAlpha == 'k')
	{
		//Letter K
		//Row 1
		//Row 2
		mvaddch(CY+1,CX+1,X);
		//Row 3
		mvaddch(CY+2,CX+1,X);
		mvaddch(CY+2,CX+3,X);
		//Row 4
		mvaddch(CY+3,CX+1,X);
		mvaddch(CY+3,CX+2,X);
		//Row 5
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+3,X);
	}
	else if(CAlpha == 'L')
	{
		//Letter L
		//Row 1
		mvaddch(CY,CX,X);
		//Row 2
		mvaddch(CY+1,CX,X);
		//Row 3
		mvaddch(CY+2,CX,X);
		//Row 4
		mvaddch(CY+3,CX,X);
		//Row 5
		mvaddch(CY+4,CX,X);
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
		mvaddch(CY+4,CX+3,X);
		mvaddch(CY+4,CX+4,X);
	}
	else if(CAlpha == 'l')
	{
		//Letter L
		//Row 1
		mvaddch(CY,CX+2,X);
		//Row 2
		mvaddch(CY+1,CX+2,X);
		//Row 3
		mvaddch(CY+2,CX+2,X);
		//Row 4
		mvaddch(CY+3,CX+2,X);
		//Row 5
		mvaddch(CY+4,CX+2,X);
	}
	else if(CAlpha == 'M')
	{
		//Letter M
		//Row 1
		mvaddch(CY+0,CX+0,X);
		mvaddch(CY+0,CX+4,X);
		//Row 2
		mvaddch(CY+1,CX+0,X);
		mvaddch(CY+1,CX+1,X);
		mvaddch(CY+1,CX+3,X);
		mvaddch(CY+1,CX+4,X);
		//Row 3
		mvaddch(CY+2,CX+0,X);
		mvaddch(CY+2,CX+2,X);
		mvaddch(CY+2,CX+4,X);
		//Row 4
		mvaddch(CY+3,CX+0,X);
		mvaddch(CY+3,CX+4,X);
		//Row 5
		mvaddch(CY+4,CX+0,X);
		mvaddch(CY+4,CX+4,X);
	}
	else if(CAlpha == 'm')
	{
		//Letter m
		//Row 1
		//Row 2
		//Row 3
		mvaddch(CY+2,CX+1,X);
		mvaddch(CY+2,CX+2,X);
		//Row 4
		mvaddch(CY+3,CX+0,X);
		mvaddch(CY+3,CX+2,X);
		mvaddch(CY+3,CX+3,X);
		//Row 5
		mvaddch(CY+4,CX+0,X);
		mvaddch(CY+4,CX+3,X);
	}
	else if(CAlpha == 'N')
	{
		//Letter N
		//Row 1
		mvaddch(CY+0,CX+0,X);
		mvaddch(CY+0,CX+4,X);
		//Row 2
		mvaddch(CY+1,CX+0,X);
		mvaddch(CY+1,CX+1,X);
		mvaddch(CY+1,CX+4,X);
		//Row 3
		mvaddch(CY+2,CX+0,X);
		mvaddch(CY+2,CX+2,X);
		mvaddch(CY+2,CX+4,X);
		//Row 4
		mvaddch(CY+3,CX+0,X);
		mvaddch(CY+3,CX+3,X);
		mvaddch(CY+3,CX+4,X);
		//Row 5
		mvaddch(CY+4,CX+0,X);
		mvaddch(CY+4,CX+4,X);
	}
	else if(CAlpha == 'n')
	{
		//Letter N
		//Row 1
		//Row 2
		mvaddch(CY+1,CX+1,X);
		mvaddch(CY+1,CX+2,X);
		//Row 3
		mvaddch(CY+2,CX+0,X);
		mvaddch(CY+2,CX+3,X);
		//Row 4
		mvaddch(CY+3,CX+0,X);
		mvaddch(CY+3,CX+3,X);
		//Row 5
		mvaddch(CY+4,CX+0,X);
		mvaddch(CY+4,CX+3,X);
	}
	else if(CAlpha == 'O')
	{
		//Letter O
		//Row 1
		mvaddch(CY,CX,' ');
		mvaddch(CY,CX+1,X);
		mvaddch(CY,CX+2,X);
		mvaddch(CY,CX+3,X);
		//Row 2
		mvaddch(CY+1,CX+0,X);
		mvaddch(CY+1,CX+4,X);
		//Row 3
		mvaddch(CY+2,CX+0,X);
		mvaddch(CY+2,CX+4,X);
		//Row 4
		mvaddch(CY+3,CX+0,X);
		mvaddch(CY+3,CX+4,X);
		//Row 5
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
		mvaddch(CY+4,CX+3,X);
	}		//Letter o
	else if(CAlpha == 'o')
	{
		//Row 1
		//Row 2
		mvaddch(CY+1,CX+1,X);
		mvaddch(CY+1,CX+2,X);
		mvaddch(CY+1,CX+3,X);
		//Row 3
		mvaddch(CY+2,CX+0,X);
		mvaddch(CY+2,CX+4,X);
		//Row 4
		mvaddch(CY+3,CX+0,X);
		mvaddch(CY+3,CX+4,X);
		//Row 5
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
		mvaddch(CY+4,CX+3,X);
	}
		//Letter P
	else if(CAlpha == 'P')
	{
		//Row 1
		mvaddch(CY+0,CX+0,X);
		mvaddch(CY+0,CX+1,X);
		mvaddch(CY+0,CX+2,X);
		//Row 2
		mvaddch(CY+1,CX+0,X);
		mvaddch(CY+1,CX+3,X);
		//Row 3
		mvaddch(CY+2,CX+0,X);
		mvaddch(CY+2,CX+1,X);
		mvaddch(CY+2,CX+2,X);
		//Row 4
		mvaddch(CY+3,CX+0,X);
		//Row 5
		mvaddch(CY+4,CX+0,X);
	}		//Letter p
	else if(CAlpha == 'p')
	{
		//Row 1
		//Row 2
		mvaddch(CY+1,CX+0,X);
		mvaddch(CY+1,CX+1,X);
		//Row 3
		mvaddch(CY+2,CX+0,X);
		mvaddch(CY+2,CX+2,X);
		//Row 4
		mvaddch(CY+3,CX+0,X);
		mvaddch(CY+3,CX+1,X);
		//Row 5
		mvaddch(CY+4,CX+0,X);
	}
		//Letter Q
	else if( CAlpha == 'Q')
	{
		//Row 1
		mvaddch(CY+0,CX+1,X);
		mvaddch(CY+0,CX+2,X);
		mvaddch(CY+0,CX+3,X);
		//Row 2
		mvaddch(CY+1,CX+0,X);
		mvaddch(CY+1,CX+4,X);
		//Row 3
		mvaddch(CY+2,CX+0,X);
		mvaddch(CY+2,CX+4,X);
		//Row 4
		mvaddch(CY+3,CX+0,X);
		mvaddch(CY+3,CX+3,X);
		mvaddch(CY+3,CX+4,X);
		//Row 5
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
		mvaddch(CY+4,CX+3,X);
		mvaddch(CY+4,CX+4,X);
	}
	else if(CAlpha == 'q')
	{
		//Row 1
		//Row 2
		mvaddch(CY+1,CX+1,X);
		mvaddch(CY+1,CX+2,X);
		//Row 3
		mvaddch(CY+2,CX+0,X);
		mvaddch(CY+2,CX+2,X);
		//Row 4
		mvaddch(CY+3,CX+1,X);
		mvaddch(CY+3,CX+2,X);
		//Row 5
		mvaddch(CY+4,CX+2,X);
	}
		//Letter R
	else if(CAlpha == 'R')
	{
		//Row 1
		mvaddch(CY+0,CX+0,X);
		mvaddch(CY+0,CX+1,X);
		mvaddch(CY+0,CX+2,X);
		//Row 2
		mvaddch(CY+1,CX+0,X);
		mvaddch(CY+1,CX+3,X);
		//Row 3
		mvaddch(CY+2,CX+0,X);
		mvaddch(CY+2,CX+1,X);
		mvaddch(CY+2,CX+2,X);
		//Row 4
		mvaddch(CY+3,CX+0,X);
		mvaddch(CY+3,CX+3,X);
		//Row 5
		mvaddch(CY+4,CX+0,X);
		mvaddch(CY+4,CX+3,X);
	}		//Letter r
	else if(CAlpha == 'r')
	{
		//Row 1
		//Row 2
		mvaddch(CY+1,CX+1,X);
		mvaddch(CY+1,CX+2,X);
		//Row 3
		mvaddch(CY+2,CX+1,X);
		mvaddch(CY+2,CX+3,X);
		//Row 4
		mvaddch(CY+3,CX+1,X);
		//Row 5
		mvaddch(CY+4,CX+1,X);
	}
	else if(CAlpha == 's' || CAlpha == 'S')
	{
		//The S Code
		//Row 1
		mvaddch(CY,CX+1,X);
		mvaddch(CY,CX+2,X);
		mvaddch(CY,CX+3,X);
		//Row 2
		mvaddch(CY+1,CX,X);
		//Row 3
		mvaddch(CY+2,CX+1,X);         
		mvaddch(CY+2,CX+2,X);
		mvaddch(CY+2,CX+3,X);
		//Row 4
		mvaddch(CY+3,CX+4,X);         
		//Row 5
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
		mvaddch(CY+4,CX+3,X);
	}
	else if(CAlpha == 'T')
	{
		//The T Code
		//Row 1
		mvaddch(CY,CX,X);
		mvaddch(CY,CX+1,X);
		mvaddch(CY,CX+2,X);
		mvaddch(CY,CX+3,X);
		mvaddch(CY,CX+4,X);
		//Row 2
		mvaddch(CY+1,CX+2,X);
		//Row 3       
		mvaddch(CY+2,CX+2,X);
		//Row 4
		mvaddch(CY+3,CX+2,X);         
		//Row 5
		mvaddch(CY+4,CX+2,X);
	}
	else if(CAlpha == 't')
	{
		//The T Code
		//Row 1
		mvaddch(CY,CX+1,X);
		//Row 2
		mvaddch(CY+1,CX+0,X);
		mvaddch(CY+1,CX+1,X);
		mvaddch(CY+1,CX+2,X);
		//Row 3       
		mvaddch(CY+2,CX+1,X);
		//Row 4
		mvaddch(CY+3,CX+1,X); 
		mvaddch(CY+3,CX+3,X);         
		//Row 5
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
	}
	else if(CAlpha == 'U')
	{
		//The U Code
		//Row 1
		mvaddch(CY,CX,X);
		mvaddch(CY,CX+4,X);
		//Row 2
		mvaddch(CY+1,CX,X);
		mvaddch(CY+1,CX+4,X);
		//Row 3       
		mvaddch(CY+2,CX,X);
		mvaddch(CY+2,CX+4,X);
		//Row 4
		mvaddch(CY+3,CX,X); 
		mvaddch(CY+3,CX+4,X);         
		//Row 5
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
		mvaddch(CY+4,CX+3,X);
	}
	else if(CAlpha == 'u')
	{
		//The U Code
		//Row 1
		//Row 2
		//Row 3       
		mvaddch(CY+2,CX+1,X);
		mvaddch(CY+2,CX+3,X);
		//Row 4
		mvaddch(CY+3,CX+1,X); 
		mvaddch(CY+3,CX+3,X);         
		//Row 5
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
		mvaddch(CY+4,CX+3,X);
	}
	else if(CAlpha == 'V')
	{
		//The V Code
		//Row 1
		mvaddch(CY,CX,X);
		mvaddch(CY,CX+4,X);
		//Row 2
		mvaddch(CY+1,CX,X);
		mvaddch(CY+1,CX+4,X);
		//Row 3       
		mvaddch(CY+2,CX+1,X);
		mvaddch(CY+2,CX+3,X);
		//Row 4
		mvaddch(CY+3,CX+1,X); 
		mvaddch(CY+3,CX+3,X);         
		//Row 5
		mvaddch(CY+4,CX+2,X);
	}
	else if(CAlpha == 'v')
	{
		//The U Code
		//Row 1
		//Row 2
		//Row 3       
		mvaddch(CY+2,CX+1,X);
		mvaddch(CY+2,CX+3,X);
		//Row 4
		mvaddch(CY+3,CX+1,X); 
		mvaddch(CY+3,CX+3,X);         
		//Row 5
		mvaddch(CY+4,CX+2,X);
	}
	else if(CAlpha == 'w' || CAlpha == 'W')
	{
		//The W Code
		//Row 1
		mvaddch(CY,CX,X);
		mvaddch(CY,CX+4,X);
		//Row 2
		mvaddch(CY+1,CX,X);
		mvaddch(CY+1,CX+4,X);
		//Row 3       
		mvaddch(CY+2,CX+0,X);
		mvaddch(CY+2,CX+2,X);
		mvaddch(CY+2,CX+4,X);
		//Row 4
		mvaddch(CY+3,CX+0,X); 
		mvaddch(CY+3,CX+2,X); 
		mvaddch(CY+3,CX+4,X);         
		//Row 5
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+3,X);
	}
	else if(CAlpha == 'w' || CAlpha == 'W')
	{
		//The W Code
		//Row 1
		//Row 2
		mvaddch(CY+1,CX,X);
		mvaddch(CY+1,CX+4,X);
		//Row 3       
		mvaddch(CY+2,CX+0,X);
		mvaddch(CY+2,CX+2,X);
		mvaddch(CY+2,CX+4,X);
		//Row 4
		mvaddch(CY+3,CX+0,X); 
		mvaddch(CY+3,CX+2,X); 
		mvaddch(CY+3,CX+4,X);         
		//Row 5
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+3,X);
	}
	else if(CAlpha == 'X')
	{
		//The X Code
		//Row 1
		mvaddch(CY,CX,X);
		mvaddch(CY,CX+4,X);
		//Row 2
		mvaddch(CY+1,CX+1,X);
		mvaddch(CY+1,CX+3,X);
		//Row 3       
		mvaddch(CY+2,CX+2,X);
		//Row 4
		mvaddch(CY+3,CX+1,X); 
		mvaddch(CY+3,CX+3,X);          
		//Row 5
		mvaddch(CY+4,CX,X);
		mvaddch(CY+4,CX+4,X);
	}
	else if(CAlpha == 'x')
	{
		//The X Code
		//Row 1
		//Row 2
		//Row 3       
		mvaddch(CY+2,CX+1,X);
		mvaddch(CY+2,CX+3,X);
		//Row 4
		mvaddch(CY+3,CX+2,X);          
		//Row 5
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+3,X);
	}
	else if(CAlpha == 'Y')
	{
		//The Y Code
		//Row 1
		mvaddch(CY,CX,X);
		mvaddch(CY,CX+4,X);
		//Row 2
		mvaddch(CY+1,CX+1,X);
		mvaddch(CY+1,CX+3,X);
		//Row 3       
		mvaddch(CY+2,CX+2,X);
		//Row 4
		mvaddch(CY+3,CX+2,X);          
		//Row 5
		mvaddch(CY+4,CX+2,X);
	}
	else if(CAlpha == 'y')
	{
		//The Y Code
		//Row 1
		//Row 2
		mvaddch(CY+1,CX+1,X);
		mvaddch(CY+1,CX+3,X);
		//Row 3       
		mvaddch(CY+2,CX+2,X);
		mvaddch(CY+2,CX+3,X);
		//Row 4
		mvaddch(CY+3,CX+3,X);          
		//Row 5
		mvaddch(CY+4,CX+2,X);
		mvaddch(CY+4,CX+3,X);
	}
	else if(CAlpha == 'Z')
	{
		//The Z Code
		//Row 1
		mvaddch(CY,CX,X);
		mvaddch(CY,CX+1,X);
		mvaddch(CY,CX+2,X);
		mvaddch(CY,CX+3,X);
		mvaddch(CY,CX+4,X);
		//Row 2
		mvaddch(CY+1,CX+3,X);
		//Row 3       
		mvaddch(CY+2,CX+2,X);
		//Row 4
		mvaddch(CY+3,CX+1,X);         
		//Row 5
		mvaddch(CY+4,CX,X);
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
		mvaddch(CY+4,CX+3,X);
		mvaddch(CY+4,CX+4,X);
	}
	else if(CAlpha == 'z')
	{
		//The Z Code
		//Row 1
		//Row 2
		mvaddch(CY+1,CX,X);
		mvaddch(CY+1,CX+1,X);
		mvaddch(CY+1,CX+2,X);
		mvaddch(CY+1,CX+3,X);
		//Row 3       
		mvaddch(CY+2,CX+2,X);
		//Row 4
		mvaddch(CY+3,CX+1,X);         
		//Row 5
		mvaddch(CY+4,CX,X);
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
		mvaddch(CY+4,CX+3,X);
	}
	else if(CAlpha == '9')
	{
		//The Z Code
		//Row 1
		mvaddch(CY,CX+1,X);
		mvaddch(CY,CX+2,X);
		mvaddch(CY,CX+3,X);
		//Row 2
		mvaddch(CY+1,CX+1,X);
		mvaddch(CY+1,CX+3,X);
		//Row 3       
		mvaddch(CY+2,CX+1,X);
		mvaddch(CY+2,CX+2,X);
		mvaddch(CY+2,CX+3,X);
		//Row 4
		mvaddch(CY+3,CX+3,X);         
		//Row 5
		mvaddch(CY+4,CX+3,X);
	}
	else if(CAlpha == '8')
	{
		//The Z Code
		//Row 1
		mvaddch(CY,CX+1,X);
		mvaddch(CY,CX+2,X);
		mvaddch(CY,CX+3,X);
		//Row 2
		mvaddch(CY+1,CX,X);
		mvaddch(CY+1,CX+4,X);
		//Row 3       
		mvaddch(CY+2,CX+1,X);
		mvaddch(CY+2,CX+2,X);
		mvaddch(CY+2,CX+3,X);
		//Row 4
		mvaddch(CY+3,CX,X); 
		mvaddch(CY+3,CX+4,X);         
		//Row 5
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
		mvaddch(CY+4,CX+3,X);
	}
	else if(CAlpha == '7')
	{
		//The Z Code
		//Row 1
		mvaddch(CY,CX,X);
		mvaddch(CY,CX+1,X);
		mvaddch(CY,CX+2,X);
		mvaddch(CY,CX+3,X);
		//Row 2
		mvaddch(CY+1,CX+3,X);
		//Row 3       
		mvaddch(CY+2,CX+2,X);
		//Row 4
		mvaddch(CY+3,CX+1,X); 
		//Row 5
		mvaddch(CY+4,CX,X);
	}
	else if(CAlpha == '6')
	{
		//The Z Code
		//Row 1
		mvaddch(CY,CX+1,X);
		mvaddch(CY,CX+2,X);
		mvaddch(CY,CX+3,X);
		//Row 2
		mvaddch(CY+1,CX,X);
		//Row 3       
		mvaddch(CY+2,CX,X);
		mvaddch(CY+2,CX+1,X);
		mvaddch(CY+2,CX+2,X);
		mvaddch(CY+2,CX+3,X);
		//Row 4
		mvaddch(CY+3,CX,X); 
		mvaddch(CY+3,CX+4,X);         
		//Row 5
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
		mvaddch(CY+4,CX+3,X);
	}
	else if(CAlpha == '5')
	{
		//The Z Code
		//Row 1
		mvaddch(CY,CX+0,X);
		mvaddch(CY,CX+1,X);
		mvaddch(CY,CX+2,X);
		mvaddch(CY,CX+3,X);
		//Row 2
		mvaddch(CY+1,CX,X);
		//Row 3       
		mvaddch(CY+2,CX,X);
		mvaddch(CY+2,CX+1,X);
		mvaddch(CY+2,CX+2,X);
		//Row 4
		mvaddch(CY+3,CX+3,X);         
		//Row 5
		mvaddch(CY+4,CX,X);
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
	}
	else if(CAlpha == '4')
	{
		//The Z Code
		//Row 1
		mvaddch(CY,CX+2,X);
		mvaddch(CY,CX+3,X);
		//Row 2
		mvaddch(CY+1,CX+2,X);
		mvaddch(CY+1,CX+3,X);
		//Row 3       
		mvaddch(CY+2,CX+1,X);
		mvaddch(CY+2,CX+3,X);
		//Row 4
		mvaddch(CY+3,CX+0,X); 
		mvaddch(CY+3,CX+1,X); 
		mvaddch(CY+3,CX+2,X); 
		mvaddch(CY+3,CX+3,X); 
		mvaddch(CY+3,CX+4,X);         
		//Row 5
		mvaddch(CY+4,CX+3,X);
	}
	else if(CAlpha == '3')
	{
		//The Z Code
		//Row 1
		mvaddch(CY,CX+1,X);
		mvaddch(CY,CX+2,X);
		mvaddch(CY,CX+3,X);
		//Row 2
		mvaddch(CY+1,CX+4,X);
		//Row 3       
		mvaddch(CY+2,CX+2,X);
		mvaddch(CY+2,CX+3,X);
		//Row 4
		mvaddch(CY+3,CX+4,X);         
		//Row 5
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
		mvaddch(CY+4,CX+3,X);
	}
	else if(CAlpha == '2')
	{
		//The Z Code
		//Row 1
		mvaddch(CY,CX+1,X);
		mvaddch(CY,CX+2,X);
		mvaddch(CY,CX+3,X);
		//Row 2
		mvaddch(CY+1,CX,X);
		mvaddch(CY+1,CX+4,X);
		//Row 3       
		mvaddch(CY+2,CX+2,X);
		mvaddch(CY+2,CX+3,X);
		//Row 4
		mvaddch(CY+3,CX+1,X);         
		//Row 5
		mvaddch(CY+4,CX,X);
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
		mvaddch(CY+4,CX+3,X);
		mvaddch(CY+4,CX+4,X);
	}
	else if(CAlpha == '1')
	{
		//The Z Code
		//Row 1
		mvaddch(CY,CX+2,X);
		//Row 2
		mvaddch(CY+1,CX+1,X);
		mvaddch(CY+1,CX+2,X);
		//Row 3       
		mvaddch(CY+2,CX+2,X);
		//Row 4
		mvaddch(CY+3,CX+2,X);         
		//Row 5
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
		mvaddch(CY+4,CX+3,X);
	}
	else if(CAlpha == '0')
	{
		//The Z Code
		//Row 1
		mvaddch(CY,CX+1,X);
		mvaddch(CY,CX+2,X);
		mvaddch(CY,CX+3,X);
		//Row 2
		mvaddch(CY+1,CX,X);
		mvaddch(CY+1,CX+4,X);
		//Row 3       
		mvaddch(CY+2,CX,X);
		mvaddch(CY+2,CX+2,X);
		mvaddch(CY+2,CX+4,X);
		//Row 4
		mvaddch(CY+3,CX,X); 
		mvaddch(CY+3,CX+4,X);         
		//Row 5
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
		mvaddch(CY+4,CX+3,X);
	}
	else if(isspace(CAlpha)) 
	{
		//Blank Space Code. Use as a space or as an overwrite.
		mvaddch(CY,CX,' ');
		mvaddch(CY,CX+1,' ');
		mvaddch(CY,CX+2,' ');
		mvaddch(CY,CX+3,' ');
		mvaddch(CY,CX+4,' ');
		mvaddch(CY+1,CX,' ');
		mvaddch(CY+1,CX+1,' ');
		mvaddch(CY+1,CX+2,' ');
		mvaddch(CY+1,CX+3,' ');
		mvaddch(CY+1,CX+4,' ');
		mvaddch(CY+2,CX,' ');
		mvaddch(CY+2,CX+1,' ');
		mvaddch(CY+2,CX+2,' ');
		mvaddch(CY+2,CX+3,' ');
		mvaddch(CY+2,CX+4,' ');
		mvaddch(CY+3,CX,' ');
		mvaddch(CY+3,CX+1,' ');
		mvaddch(CY+3,CX+2,' ');
		mvaddch(CY+3,CX+3,' ');
		mvaddch(CY+3,CX+4,' ');
		mvaddch(CY+4,CX,' ');
		mvaddch(CY+4,CX+1,' ');
		mvaddch(CY+4,CX+2,' ');
		mvaddch(CY+4,CX+3,' ');
		mvaddch(CY+4,CX+4,' ');
	}
	else
	{
		//Character does not exist code
		//row 1
		mvaddch(CY,CX,X);
		mvaddch(CY,CX+1,X);
		mvaddch(CY,CX+2,X);
		mvaddch(CY,CX+3,X);
		mvaddch(CY,CX+4,X);
		//row 2
		mvaddch(CY+1,CX,X);
		mvaddch(CY+1,CX+1,X);
		mvaddch(CY+1,CX+3,X);
		mvaddch(CY+1,CX+4,X);
		//row 3
		mvaddch(CY+2,CX,X);
		mvaddch(CY+2,CX+2,X);
		mvaddch(CY+2,CX+4,X);
		//row 4
		mvaddch(CY+3,CX,X);
		mvaddch(CY+3,CX+1,X);
		mvaddch(CY+3,CX+3,X);
		mvaddch(CY+3,CX+4,X);
		//row 5
		mvaddch(CY+4,CX,X);
		mvaddch(CY+4,CX+1,X);
		mvaddch(CY+4,CX+2,X);
		mvaddch(CY+4,CX+3,X);
		mvaddch(CY+4,CX+4,X);
	}
}