#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curses.h>

#include <miniat/miniat.h>
#include "p_dpad.h"

#define SIGNBOARD_UP 1
#define SIGNBOARD_DOWN 2
#define SIGNBOARD_LEFT 3
#define SIGNBOARD_RIGHT 4
#define NO_KEY 0


struct p_dpad {
	int connected;
	m_uword address;
	m_bus *bus;
};

p_dpad *p_dpad_new(m_uword address) {

	p_dpad *k = (p_dpad *)malloc(sizeof(p_dpad));
	if(k) {
		k->bus = (m_bus *)malloc(sizeof(m_bus));
		if(!k->bus) {
			free(k);
			k = NULL;
		}
		else {
			k->address = address;
			k->connected = 0;
		}
	}
	return k;
}

void p_dpad_bus_connector_set(p_dpad *k, m_bus *bus) {

	if(k && bus) {
		if(!k->connected) {
			free(k->bus);
		}
		k->bus = bus;
		k->connected = 1;
	}
	return;
}

void p_dpad_free(p_dpad *k) {

	if(k) {
		if(!k->connected) {
			free(k->bus);
		}
		free(k);
	}
	return;
}

void p_dpad_clock(p_dpad *k) {

	int key;
	if(!k) {
		return;
	}

	/*
	 * If the miniat is making a request, has the address set and is reading
	 * we ack that we have received the request and take the data off the bus
	 */
	if(k->bus->req && (k->bus->address == k->address) && !k->bus->rW && !k->bus->ack) {
		k->bus->ack = M_HIGH;
		//CURSES INFO
		/*Detects i key is being pressed and sends relevant flag through bus*/
		keypad(stdscr, TRUE);
		nodelay(stdscr, TRUE);
		key = getch();
		switch(key) {		
		case KEY_UP: 
			k->bus->data = SIGNBOARD_UP;		
			break; 
		case KEY_DOWN: 
			k->bus->data = SIGNBOARD_DOWN;
			break; 
		case KEY_LEFT: 
			k->bus->data = SIGNBOARD_LEFT;
			break; 
		case KEY_RIGHT: 
			k->bus->data = SIGNBOARD_RIGHT;
			break; 
		default:
			k->bus->data = NO_KEY;
			break;
		}
	}
	/* If we ack'd bring it back low */
	else if(k->bus->ack && (k->bus->address == k->address)) {
		k->bus->ack = M_LOW;
	}
	return;
}

m_bus p_dpad_get_bus(p_dpad *k) {
	m_bus b = { 0 };
	if(!k) {
		return b;
	}
	return *(k->bus);
}

void p_dpad_set_bus(p_dpad * k, m_bus bus) {

	if(k) {
		memcpy(k->bus, &bus, sizeof(bus));
	}
}
