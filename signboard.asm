.constant	PORTA	0x1B1A
.constant   BUTN    0x4000
.constant	C_P	    0x4001 
.constant   S_T     0x4002 

.constant   C_POS   0x4003 

.constant   T_L     0x4004
.constant   W_X		0x4005
.constant   X_CHAR  0x4011
.constant   B_PRINT 0x4012
.constant   COL 	0x4016
.constant   FC 		0x4017

.var i
.var j
.var length

.address 0x2000
!main
	STOR r0, [C_POS]
	MOVI r15, 9
	STOR r15, [COL]
	MOVI r1, 'o'
	STOR r1, [FC]
	MOVI r1, !string
	NOP
	BRA [!send]



!send
	LOAD r3, [r1]
	BRAE r3, r0 [!start]	
	STOR r3, [S_T]
	LOAD r4, [C_POS]
	ADD  r4, r4, (1)
	STOR r4, [C_POS]
	STOR r4, [T_L]		
	ADD r1, r1, (1)	
	BRA [!send]

!start
	STOR r0,  [i]
	MOVI r2,  17
	STOR r2,  [j]
	LOAD r10, [T_L]	

!loopi	
	LOAD r2, [PORTA] 
	BRAE r2, r0 [!loopi]
	LOAD r3, [i]

	BRAE r3, r10 [!done] 

!loopj
		
	LOAD r4, [j]
	BRAE r4, r3 [!li]
	MOVR r6, r4
	ADD  r6, r6 (1)
	STOR r6, [X_CHAR]
	MOVI r7, ' '
	STOR r7, [C_P]
	STOR r4, [X_CHAR]
	STOR r3, [C_POS]
	LOAD r9, [S_T]
	STOR r9, [C_P]
	MOVR r8, r0
	MOVI r5, 500

!sleep
			
	BRAE r8, r5 [!continue]
	ADD  r8, r8, (1)
	BRA [!sleep]
!continue
			
			
	SUB  r4, r4, (1) 
	STOR r4, [j]
	BRA[!loopj]
!li
	ADD r3, r3, (1)
	STOR r3, [i]
	MOVI r2,  17
	STOR r2,  [j]
	BRA [!loopi]

!done	
	BRA [!main]


!string
	"HELLO SIGNBOARD"
