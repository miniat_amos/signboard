#include <stdio.h>
#include <curses.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "miniat/miniat.h"
#include "cursor_print.h"
#include "store_text.h"
#include "cursor_position.h"
#include "text_length.h"
#include "p_dpad.h"
#include "window_x.h"
#include "clear_screen.h"
#include "x_char.h"
#include "buffer_print.h"
#include "column.h"
#include "row.h"
#include "window_y.h"
#include "color.h"
#include "fontCharacter.h"
#include "font.h"
#include "single_char.h"
#include "buffer_print_curve.h"

#define M_DPAD_ADDRESS 			0x4000
#define M_SIMPLE_C_P_ADDRESS    0x4001
#define M_SIMPLE_S_T_ADDRESS    0x4002
#define CURSOR_POSITION_ADDRESS 0x4003
#define TEXT_LENGTH_ADDRESS 	0x4004
#define WINDOW_X_ADDRESS		0x4005
#define SCREEN_ADDRESS			0x4010
#define X_CHAR_ADDRESS			0x4011
#define BUFFER_PRINT_ADDRESS	0x4012
#define COLUMN_ADDRESS			0x4013
#define ROW_ADDRESS				0x4014
#define WINDOW_Y_ADDRESS		0x4015
#define COLOR 					0x4016
#define FONT_CHAR 				0x4017
#define SINGLE_CHAR             0x4018
#define BUFFER_PRINT_CURVE_ADDRESS   	0x4019

miniat *m = NULL; //initializes miniat
p_dpad *d = NULL;// initialize dpad
p_cursor_print *c_p = NULL; //initializes cursor print
p_store_text *s_t = NULL;//initialiazes stored text
p_cursor_pos *c_pos = NULL;//initializes cursor position
p_text_length *t_l = NULL;//initializes length of text
window_x *w_x=NULL; //initializes x coordinate
window_y *w_y=NULL; //initializes y coordinate
clear_screen    *screen=NULL; //initializes clear_screen
x_char *xc_pos =NULL; //initializes x_char
buffer_print *b_f =NULL;//initializes buffer_print
p_column *c_m =NULL;//initializes column
p_row *r_w =NULL;//initializes row
color *col =NULL;//initializes color
fontChar *fc =NULL;//initializes color
p_single_char *s_c = NULL;
buffer_print_curve *bpc = NULL;

static void cleanup();//initializes cleanup function

int main(int argc, char *argv[]) { //activates main program
	char *fontinfo;
	if(argc < 2) {
		fprintf(stderr, "You must specify a bin file name\n\n");
		exit(1);
	}
	if (argc >= 3) {
		if(strcmp(argv[2], "-f") == 0) {
			if(argc >= 4) {
				fontinfo = argv[3];
			}
			else {
				fontinfo = "font.pbm";
				printf("You used the flag -f to define your font but didn't provide a font file; attempting to default to font.pbm\n");
			}
		}
		else
			fontinfo = "font.pbm";
	}
	else
		fontinfo = "font.pbm";


	char* pPath;
        int pathlength;
        pPath = getenv("SIGNBOARDPATH");
	if(!pPath) {
		printf("\n\nYou don't seem to have an environment variable named \"SIGNBOARDPATH\"\nPlease create this variable with the path to your font.\nPlease see the README.md file in the Bitbucket repository for more info.\n\n");
		exit(1);
	}
	
        strcat(pPath, fontinfo);
        ourfont = load_font(pPath);
        if(!ourfont) {
                printf("Your font didn't load properly, please check your environment variables. \n");
                exit(1);
        }


	int key, n; //initializes variables key and n
	bool started = false;
	int iterator = 0; //ensure enough cycles pass for components to be initialized
	FILE *f = fopen(argv[1], "rb");
	if(!f)
	{
		fprintf(stderr, "File %s could not be opened \"%s\"\n\n", argv[1], argv[1]);
		exit(1);
	}
	m = miniat_new(f);//initializes new miniat setup
	m_bus bus_state; // bus state from miniat */
	m_bus peripheral_bus_state; // bus state from peripherals */

	m_wyde portA;

	atexit(cleanup); //clean up program when exiting

	d = p_dpad_new(M_DPAD_ADDRESS);
	c_p= p_cursor_print_new(M_SIMPLE_C_P_ADDRESS); //sets cursor print to new address
	s_t= p_store_text_new(M_SIMPLE_S_T_ADDRESS); //set store text to new address
	c_pos= p_cursor_pos_new(CURSOR_POSITION_ADDRESS); //creates new cursor position
	t_l=p_text_length_new(TEXT_LENGTH_ADDRESS);////sets new text length
	w_x=window_x_new(WINDOW_X_ADDRESS); //sets new window size
	w_y=window_y_new(WINDOW_Y_ADDRESS);
	screen=clear_screen_new(SCREEN_ADDRESS); //
	xc_pos=x_char_new(X_CHAR_ADDRESS); //
	b_f=buffer_print_new(BUFFER_PRINT_ADDRESS);//
	bpc=buffer_print_curve_new(BUFFER_PRINT_CURVE_ADDRESS);//
	c_m=p_column_new(COLUMN_ADDRESS);//
	r_w=p_row_new(ROW_ADDRESS);//
	col= color_new(COLOR); //creates new color
	fc= fontCharacter_new(FONT_CHAR);
	s_c=p_single_char_new(SINGLE_CHAR);//

	WINDOW * mainwin; //creates window


	if ((mainwin = initscr()) == NULL) { //if main window fails
	fprintf(stderr, "Error initializing ncurses.\n"); //print error message
	exit(EXIT_FAILURE); //and exit program
	}
	curs_set(0);

	while(1)//initiates while loop
	{
		miniat_clock(m);//sets value of miniat clock


		portA = miniat_pins_get_gpio_port(m, m_gpio_id_A);//gets port A


		bus_state = miniat_pins_bus_get(m); //gets current pins of bus and sets them as the bus state

		p_dpad_set_bus(d, bus_state);
		p_store_text_set_bus(s_t, bus_state); //store text
		p_cursor_print_set_bus(c_p, bus_state); // prepares cursor to print
		p_cursor_pos_set_bus(c_pos, bus_state); //set cursor position
		p_text_length_set_bus(t_l, bus_state); //sets text length
		window_x_set_bus(w_x, bus_state); //sets x coordinate to the bus
		window_y_set_bus(w_y, bus_state); //sets y coordinate to the bus
		clear_screen_set_bus(screen, bus_state); //sets clear screen
		x_char_set_bus(xc_pos, bus_state); //
		buffer_print_set_bus(b_f, bus_state);
		buffer_print_curve_set_bus(bpc, bus_state);
		p_column_set_bus(c_m, bus_state);
		p_row_set_bus(r_w, bus_state);
		color_set_bus(col, bus_state);
		fontCharacter_set_bus(fc, bus_state);
		p_single_char_set_bus(s_c, bus_state);

		keypad(stdscr, TRUE); //sets up keypad
		nodelay(stdscr, TRUE); //sets up nodelay
		noecho(); //no characters from keyboard printed to screen
		key = getch(); //gets input for key


		switch(key) //switch design for keypresses
		{
			case 112: //if "p" key is pressed
			portA.bits.bit0 = 1; //activates port A
			miniat_pins_set_gpio_port(m, m_gpio_id_A, portA); //sets pin for port A
			break; //break here

			case 111: // if "o" key is pressed
			portA.bits.bit0 = 0; //activates port A
			miniat_pins_set_gpio_port(m, m_gpio_id_A, portA); //sets pin for port A
			break; //breaks here
		}

		if(!started && iterator > 25) {
			portA.bits.bit0 = 1;
			miniat_pins_set_gpio_port(m, m_gpio_id_A, portA);
			started = true;
		}
		


		if(portA.bits.bit0==0){ clear(); refresh();} 

		p_dpad_clock(d);
		p_store_text_clock(s_t);
		p_cursor_print_clock(c_p);
		p_cursor_pos_clock(c_pos);
		p_text_length_clock(t_l);
		window_x_clock(w_x);
		window_y_clock(w_y);
		clear_screen_clock(screen);
		x_char_clock(xc_pos);
		buffer_print_clock(b_f);
		buffer_print_curve_clock(bpc);
		p_column_clock(c_m);
		p_row_clock(r_w);
		color_clock(col);//
		fontCharacter_clock(fc);//
		p_single_char_clock(s_c);



		/*
		 * Transfer the data from the peripheral back to the miniat, we need to do
		 * transfer these by current address on the miniat "bus" since the miniat
		 * listens for everything. We wouldn't want an inactive peripherals data
		 * to clobber the active peripherals data on the wire.
		 */
		peripheral_bus_state = p_dpad_get_bus(d); //sets peripheral bus stat to c_p

		if(peripheral_bus_state.address == M_DPAD_ADDRESS ) { //if peripheral bus state address = original c_p address
			goto set_address;
		}

		peripheral_bus_state = p_cursor_print_get_bus(c_p); //sets peripheral bus stat to c_p

		if(peripheral_bus_state.address == M_SIMPLE_C_P_ADDRESS) { //if peripheral bus state address = original c_p address
			goto set_address;
		}

		peripheral_bus_state = p_store_text_get_bus(s_t); //sets peripheral bus state to s_t

		if(peripheral_bus_state.address == M_SIMPLE_S_T_ADDRESS) { //if peripheral bus state = original s_t
			goto set_address;
		}

		peripheral_bus_state = p_cursor_pos_get_bus(c_pos); //sets peripheral bus state to cur_pos

		if(peripheral_bus_state.address == CURSOR_POSITION_ADDRESS) { //if peripheral bus state = original cur_pos
			goto set_address;
		}

		peripheral_bus_state = p_text_length_get_bus(t_l); //sets peripheral bus state to text length

		if(peripheral_bus_state.address == TEXT_LENGTH_ADDRESS) { //if peripheral bus state = original text length
			goto set_address;
		}

		peripheral_bus_state = window_x_get_bus(w_x); //sets peripheral bus state to window x

		if(peripheral_bus_state.address == WINDOW_X_ADDRESS) { //if peripheral bus state = original window x
			goto set_address;
		}

		peripheral_bus_state = window_y_get_bus(w_y); //sets peripheral bus state to window x

		if(peripheral_bus_state.address == WINDOW_Y_ADDRESS) { //if peripheral bus state = original window x
			goto set_address;
		}

		peripheral_bus_state = clear_screen_get_bus(screen); //sets peripheral bus state to clear_screen

		if(peripheral_bus_state.address == SCREEN_ADDRESS) { //if peripheral bus state = original SCREEN_ADDRESS
			goto set_address;
		}
		peripheral_bus_state = x_char_get_bus(xc_pos); //sets peripheral bus state to x_char

		if(peripheral_bus_state.address == X_CHAR_ADDRESS) { // if peripheral bus state = original X_CHAR_ADDRESS
			goto set_address;
		}

		peripheral_bus_state = buffer_print_get_bus(b_f);

		if(peripheral_bus_state.address == BUFFER_PRINT_ADDRESS) {
			goto set_address;
		}

		peripheral_bus_state = buffer_print_curve_get_bus(bpc);

		if(peripheral_bus_state.address == BUFFER_PRINT_CURVE_ADDRESS) {
			goto set_address;
		}

		peripheral_bus_state = p_column_get_bus(c_m);

		if(peripheral_bus_state.address == COLUMN_ADDRESS) {
			goto set_address;
		}

		peripheral_bus_state = p_row_get_bus(r_w);

		if(peripheral_bus_state.address == ROW_ADDRESS) {
			goto set_address;
		}


		peripheral_bus_state = color_get_bus(col); //sets peripheral bus state to cur_pos

		if(peripheral_bus_state.address == COLOR) { //if peripheral bus state = original cur_pos
			goto set_address;
		}
		peripheral_bus_state = fontCharacter_get_bus(fc); //sets peripheral bus state to cur_pos

		if(peripheral_bus_state.address == FONT_CHAR) { //if peripheral bus state = original cur_pos
			goto set_address;
		}

        peripheral_bus_state = p_single_char_get_bus(s_c);

		if(peripheral_bus_state.address == SINGLE_CHAR) {
			goto set_address;
		}

		/* Ignore address 0, as it is a 'return to 0' bus design, 0 may be on the bus */
		if (peripheral_bus_state.address == 0) {
			continue;
		}

		/* Error if we got here log it, and keep going! The ugly cast keeps the compiler happy*/
		fprintf(stderr, "Could not find peripheral mapped to address: %lu\n",
												(long unsigned int)peripheral_bus_state.address);

		continue;
		/*
		 * If we got here, we just need to transfer the active peripherals data back to the miniat
		 * so it can process it.
		 */
set_address:
		miniat_pins_bus_set(m, peripheral_bus_state); //sets miniat pins to peripheral bus state
		iterator++;
	}
	p_dpad_free(d);
	p_cursor_print_free(c_p);
	p_store_text_free(s_t);
	p_cursor_pos_free(c_pos);
	p_text_length_free(t_l);
	window_x_free(w_x);
	window_y_free(w_y);
	clear_screen_free(screen);
	x_char_free(xc_pos);
	buffer_print_free(b_f);
	buffer_print_curve_free(bpc);
	p_column_free(c_m);
	p_row_free(r_w);
	color_free(col);
	fontCharacter_free(fc);
	p_single_char_free(s_c);
	miniat_free(m);
}

static void cleanup() {

	if(m) {  //if miniat exists
		miniat_free(m); //set miniat to free
	}
	if(d){
		p_dpad_free(d);
	}
	if(c_p) { //if  c_p exists
		p_cursor_print_free(c_p);//set c_p to free
	}
	if(s_t) { //if s_t exists
		p_store_text_free(s_t); //set s_t to free
	}
    if(s_c) { //if s_c exists
		p_single_char_free(s_c); //set s_c to free
	}
	if(c_pos) { //if c_pos exists
		p_cursor_pos_free(c_pos); //set c_pos to free
	}
	if(t_l) { //if t_l exists
		p_text_length_free(t_l); //set t_l to free
	}
	if(w_x) { //if w_x exists
		window_x_free(w_x); //set window_x to free
	}
	if(w_y) { //if w_x exists
		window_y_free(w_y); //set window_x to free
	}
	if(screen){ //if screen exists
		clear_screen_free(screen); //set clear_screen to free
	}
	if(xc_pos) { //if xc_pos exists
		x_char_free(xc_pos); //set x_char to free
	}
	if(b_f){
		buffer_print_free(b_f);
	}
	if(bpc){
		buffer_print_curve_free(bpc);
	}
	if(c_m){
		p_column_free(c_m);
	}
	if(r_w){
		p_row_free(r_w);
	}
	if(col){
		color_free(col);
	}
	if(fc){
		fontCharacter_free(fc);
	}
}
