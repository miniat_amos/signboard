.constant	PORTA	0x1B1A
.constant	C_P	    0x4001 
.constant   S_T     0x4002 

.constant   C_POS   0x4003 

.constant   T_L     0x4004
.constant   W_X		0x4005
.constant   SCREEN  0X4010
.constant   X_CHAR  0x4011
.constant   B_PRINT 0x4012
.constant   WINY    0x4015

.var i
.var j
.var length

.address 0x2000
!main
	STOR r0, [C_POS]
	STOR r0, [T_L]
	MOVI r100, 10
	STOR r100, [WINY]
	MOVI r1, !string
	NOP
	BRA [!send]



!send
	LOAD r3, [r1]
	BRAE r3, r0 [!start]	
	STOR r3, [S_T]
	LOAD r4, [C_POS]
	ADD  r4, r4, (1)
	STOR r4, [C_POS]
	STOR r4, [T_L]		
	ADD r1, r1, (1)	
	BRA [!send]

!start
	STOR r0, [SCREEN]	
	MOVI r1, 15
	STOR r1,  [i]
	MOVI r2,  1
	STOR r2,  [j]
	LOAD r3,  [T_L]
	ADD  r3, r3, (1)
	
!loop	
	LOAD r5, [PORTA] 
	BRAE r5, r0 [!loop]
	
	LOAD r4, [j]
	
	
	LOAD r2, [i]
	STOR r2, [X_CHAR]
	STOR r4, [B_PRINT]
	
	ADD r4, r4, (1)
	
	BRAE r3, r4 [!done] 
	STOR r4, [j]

	
	MOVI r5, 1500

!sleep
			
	BRAE r5, r0 [!continue]
	SUB  r5, r5, (1)
	BRA [!sleep]

!continue
			
	STOR r0, [SCREEN]	

	SUB  r2, r2, (1)
	STOR r2,  [i]
	BRA [!loop]

!done
	
	MOVI r5, 10000

!wait
			
	BRAE r5, r0 [!start]
	SUB  r5, r5, (1)
	BRA [!wait]



!string
	"HELLO SIGNBOARD"
