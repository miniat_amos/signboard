.constant 	PORTA 		 0x1B1A
.constant   BUTN    	 0x4000
.constant 	C_P     	 0x4001 
.constant   S_T     	 0x4002 

.constant   C_POS   	 0x4003 
.constant   T_L     	 0x4004
.constant   W_X  		 0x4005
.constant   X_CHAR  	 0x4011
.constant   B_PRINT_CURV 0x4019
.constant   COL  		 0x4016
.constant   FC 			 0x4017



.var i
.var j
.var length



.address 0x2000

!main
 STOR r0, [C_POS]
 MOVI r15, 9
 STOR r15, [COL]
 MOVI r1, 'o'
 STOR r1, [FC]
 MOVI r1, !string
 NOP

 BRA [!send]



!send
 LOAD r3, [r1]
 BRAE r3, r0 [!start] 
 STOR r3, [S_T]
 LOAD r4, [C_POS]
 ADD  r4, r4, (1)
 STOR r4, [C_POS]
 STOR r4, [T_L]  
 ADD r1, r1, (1) 
 BRA [!send]



!start
 STOR r0, [X_CHAR]
 MOVI r99, 0
 STOR r99, [B_PRINT_CURV]
 BRA [!main]


!string
 "HELLO SIGNBOARD"
