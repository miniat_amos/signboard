#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curses.h>

#include <miniat/miniat.h>
#include "cursor_print.h"
#include "font.h"
#include "window_x.h"
#include "window_y.h"
#include "fontCharacter.h"
#include "single_char.h"

struct p_single_char {
	int connected;
	m_uword address;
	m_bus *bus;
	char CAlpha;
};

p_single_char *p_single_char_new(m_uword address) {




	p_single_char *t = malloc(sizeof(p_single_char));
	if(t) {



		t->bus = (m_bus *)malloc(sizeof(m_bus));
		if(!t->bus) {
			free(t);
			t = NULL;
		}
		else {
			t->connected = 0;
			t->address = address;

		}
	}
	return t;
}

void p_single_char_bus_connector_set(p_single_char *t, m_bus *bus) {

	if(t && bus) {
		if(!t->connected) {
			free(t->bus);
		}
		t->bus = bus;
		t->connected = 1;
	}
	return;
}

void p_single_char_free(p_single_char *t) {


	if(t) {
		if(!t->connected) {
			free(t->bus);

		}
		free(t);
	}
	return;
}

void p_single_char_clock(p_single_char *t) {

	if(!t) {
		return;
			}

	/*
	 * If the miniat is making a request, has the address set and is writing
	 * we ack that we have received the request and take the data off the bus
	 */
	if(t->bus->req && (t->bus->address == t->address) && !t->bus->ack) {
		t->bus->ack = M_HIGH;
		if(t->bus->rW){	//f rW flag is set in bus
			if(t->bus->data){

			t->CAlpha = t->bus->data; //sets bus contents as CAlpha

			mvprintw(win_y, win_x, "%c",t->bus->data);



		}

        }

	}


	/* If we ack'd bring it back low */
	else if((t->bus->address == t->address) && t->bus->ack) {
		t->bus->ack = M_LOW;
	}
	return;
}

m_bus p_single_char_get_bus(p_single_char *t) {
	m_bus b = { 0 };
	if(!t) {
		return b;
	}
	return *(t->bus);
}

void p_single_char_set_bus(p_single_char * t, m_bus bus) {

	if(t) {
		memcpy(t->bus, &bus, sizeof(bus));
	}
}
