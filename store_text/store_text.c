#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curses.h>

#include <miniat/miniat.h>
#include "store_text.h"
#include "cursor_position.h"
#include "text_length.h"

struct p_store_text {
	int connected; //initalizes connected
	m_uword address;
	m_bus *bus;
};
char array[15]; //initializes array

p_store_text *p_store_text_new(m_uword address) { //function call




	p_store_text  *t = malloc(sizeof(p_store_text )); //sets up pointer *t of type p_store_text
	if(t) {



		t->bus = (m_bus *)malloc(sizeof(m_bus));//allocates size
		if(!t->bus) { //if that did not happen
			free(t); //free up t
			t = NULL; //and make it NULL
		}
		else {
			t->connected = 0; //t connected to 0
			t->address = address; //assign t->address an address

		}
	}
	return t;
}

void p_store_text_bus_connector_set(p_store_text  *t, m_bus *bus) { //function call

	if(t && bus) {  //if both t and bus
		if(!t->connected) { //if t-> does not exist
			free(t->bus); //frees up t->bus
		}
		t->bus = bus; //equalizes t->bus and bus
		t->connected = 1;//sets t->connected to 1
	}
	return;
}

void p_store_text_free(p_store_text *t) { //function call

	if(t) { //if t exists
		if(!t->connected) {//if t-> does not exist
			free(t->bus);//frees up t->bus
		}
		free(t); //sets t to free
	}
	return;
}

void p_store_text_clock(p_store_text *t) { //function call

	if(!t) {
		return;
			}

	/*
	 * If the miniat is making a request, has the address set and is writing
	 * we ack that we have received the request and take the data off the bus
	 */
	if(t->bus->req && (t->bus->address == t->address) && !t->bus->ack) { //if t->bus->req and t->bus->address both equal t->address and t->bus->ack does not exist
		t->bus->ack = M_HIGH; //sets t->bus->ack to M_HIGH
		if(t->bus->rW){ //if t->bus-rW exists
			if(position < 15){ //if position is less than 15
				array[position]=(char)t->bus->data; //sets the array at position to t->bus->data

			}
			else{
				printw("Too many letters\n"); //error report
			}
        }
        else{
        	if(position < text_length){ //if position is less than the end of the text
        		t->bus->data=array[position]; //sets t->bus->data to position
       	 	}
       	 	else if(text_length == position){ //if t->bus->data = [position
       	 		t->bus->data='\0'; //sets t->bus->data to 0
       	 	}
       	 	else {
       	 		printf("Out of an array length\n"); //reports error
       	 		position=0; //resets position
       	 	}
        }

	}


	/* If we ack'd bring it back low */
	else if((t->bus->address == t->address) && t->bus->ack) {
		t->bus->ack = M_LOW;
	}
	return;
}

m_bus p_store_text_get_bus(p_store_text *t) { //function call
	m_bus b = { 0 }; //initializes to 0
	if(!t) { //if t does not exist
		return b;
	}
	return *(t->bus); //otherwise return t->bus
}

void p_store_text_set_bus(p_store_text * t, m_bus bus) {

	if(t) {
		memcpy(t->bus, &bus, sizeof(bus)); //runs memcpy
	}
}
