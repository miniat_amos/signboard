.constant PORTA 0x1B1A
.constant C_P 0x4001
.constant S_T 0x4002
.constant C_POS	0x4003
.constant T_L 0x4004
.constant W_X 0x4005

.address 0x2000

!main
	STOR r0, [C_POS]
	STOR r0, [T_L]
	MOVI r1, !string

!send
	LOAD r3, [r1]
	BRAE r3, r0 [!reset]
	STOR r3, [S_T]
	LOAD r4,  [C_POS]
	ADD r4, r4, (1)
	STOR r4, [C_POS]
	STOR r4, [T_L]
	ADD r1, r1, (1)
	BRA [!send]

!reset
	STOR r0, [C_POS]

!loop
	LOAD r2, [PORTA]
	BRAE r2, r0 [!loop]

	LOAD r4, [C_POS]
	LOAD r6, [T_L]
	BRAE r4, r6 [!loop]

	LOAD r5, [S_T]
	STOR r5, [C_P]
	ADD r4, r4, (1)
	STOR r4, [C_POS]
	LOAD r6, [W_X]
	ADD  r6, r6, (8)
	STOR r6, [W_X]
	BRA [!loop]
!string
"Hello STOR"


