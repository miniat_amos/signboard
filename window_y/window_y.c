#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curses.h>

#include <miniat/miniat.h>
#include "window_y.h"

struct window_y {
	int connected;
	m_uword address;
	m_bus *bus;	

};
int win_y = 10;

window_y *window_y_new(m_uword address) {
		
		
	

	window_y  *t = malloc(sizeof(window_y ));
	if(t) {

		

		t->bus = (m_bus *)malloc(sizeof(m_bus));
		if(!t->bus) {
			free(t);
			t = NULL;
		}
		else {
			t->connected = 0;
			t->address = address;		
		}
	}
	return t;
}

void window_y_bus_connector_set(window_y  *t, m_bus *bus) {

	if(t && bus) {
		if(!t->connected) {
			free(t->bus);
		}
		t->bus = bus;
		t->connected = 1;
	}
	return;
}

void window_y_free(window_y *t) {

	if(t) {
		if(!t->connected) {
			free(t->bus);
		}
		free(t);
	}
	return;
}

void window_y_clock(window_y *t) {
	if(!t) {
		return;
			}
	
	/*
	 * If the miniat is making a request, has the address set and is writing
	 * we ack that we have received the request and take the data off the bus
	 */
	if(t->bus->req && (t->bus->address == t->address) && !t->bus->ack) {
		t->bus->ack = M_HIGH;

		if(t->bus->rW){	//if rW flag is set in bus
			if(t->bus->data >= 0 && t->bus->data < 30){ //if win_y is within boundaries
			win_y = t->bus->data; //gets win_y from bus
}
			else{
				win_y=10;
				mvprintw(10,10, "%s", "Y coordinate out of range\n");//sends win_y through bus
			}
       		 }
        	else{
        		t->bus->data=win_y;        	
       		}

        }


	/* If we ack'd bring it back low */
	else if((t->bus->address == t->address) && t->bus->ack) {
		t->bus->ack = M_LOW;
	}
	return;
}

m_bus window_y_get_bus(window_y *t) {
	m_bus b = { 0 };
	if(!t) {
		return b;
	}
	return *(t->bus);
}

void window_y_set_bus(window_y * t, m_bus bus) {

	if(t) {
		memcpy(t->bus, &bus, sizeof(bus));
	}
}
