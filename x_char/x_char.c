#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curses.h>

#include <miniat/miniat.h>
#include "x_char.h"
#include "window_x.h"

struct x_char {
	int connected;
	m_uword address;
	m_bus *bus;	
};

int char_pos;

x_char *x_char_new(m_uword address) {
		
		
	

	x_char  *t = malloc(sizeof(x_char ));
	if(t) {
		char_pos=0;
		

		t->bus = (m_bus *)malloc(sizeof(m_bus));
		if(!t->bus) {
			free(t);
			t = NULL;
		}
		else {
			t->connected = 0;
			t->address = address;		
			
		}
	}
	return t;
}

void x_char_bus_connector_set(x_char  *t, m_bus *bus) {

	if(t && bus) {
		if(!t->connected) {
			free(t->bus);
		}
		t->bus = bus;
		t->connected = 1;
	}
	return;
}

void x_char_free(x_char *t) {

	if(t) {
		if(!t->connected) {
			free(t->bus);
		}
		free(t);
	}
	return;
}

void x_char_clock(x_char *t) {
	
	if(!t) {
		return;
			}
	
	/*
	 * If the miniat is making a request, has the address set and is writing
	 * we ack that we have received the request and take the data off the bus
	 */
	if(t->bus->req && (t->bus->address == t->address) && !t->bus->ack) {
		t->bus->ack = M_HIGH;
		if(t->bus->rW){	//if rW flag is set in bus
			char_pos = (int)t->bus->data;	//gets char_pos from bus
			if(char_pos >=0){
				win_x = (char_pos*8); //sets win_x to char_pos multiplied by 8
			}
			else{
				printw("Number can't be negative\n");
			}
        }
        else{        	
        		t->bus->data = (char)char_pos;   //sends char_pos through bus        	
       	 	}
       	 	
        }
       
	
	
	
	/* If we ack'd bring it back low */
	else if((t->bus->address == t->address) && t->bus->ack) {
		t->bus->ack = M_LOW;
	}
	return;
}

m_bus x_char_get_bus(x_char *t) {
	m_bus b = { 0 };
	if(!t) {
		return b;
	}
	return *(t->bus);
}

void x_char_set_bus(x_char * t, m_bus bus) {

	if(t) {
		memcpy(t->bus, &bus, sizeof(bus));
	}
}
